## 📓 Authors

- [Rodrigo Mungo](https://gitlab.com/rodrigomungo)

## Unreleased

**Release date:** YYYY-MM-DD

### 🎉 Features

- **Initial scaffolding**
- **ChatBot**
